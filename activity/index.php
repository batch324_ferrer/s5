<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
</head>
<body>
    <?php
    session_start();

    // Check if user is already logged in
    if (isset($_SESSION['username'])) {
        header("Location: hello.php");
        exit;
    }

    // Handle login form submission
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST["username"];
        $password = $_POST["password"];

        // Check credentials (Replace this with your desired credentials)
        if ($username === "johnsmith@gmail.com" && $password === "1234") {
            $_SESSION['username'] = $username;
            header("Location: hello.php");
            exit;
        } else {
            echo "Invalid credentials. Please try again.";
        }
    }
    ?>

    <h2>Login Page</h2>
    <form method="post" action="">
        <label for="username">Username:</label>
        <input type="text" name="username" id="username" required>

        <label for="password">Password:</label>
        <input type="password" name="password" id="password" required>

        <input type="submit" value="Login">
    </form>
</body>
</html>