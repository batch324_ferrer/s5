<!DOCTYPE html>
<html>
<head>
    <title>Hello Page</title>
</head>
<body>
    <?php
    session_start();

    // Check if user is not logged in, redirect to login page
    if (!isset($_SESSION['username'])) {
        header("Location: index.php");
        exit;
    }

    // Logout button handler
    if (isset($_POST['logout'])) {
        session_destroy();
        header("Location: index.php");
        exit;
    }
    ?>

    <h2>Hello, <?php echo $_SESSION['username']; ?>!</h2>
    <form method="post" action="">
        <input type="submit" name="logout" value="Logout">
    </form>
</body>
</html>