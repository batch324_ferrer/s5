<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S05: Client-Server Communication (Discussion)</title>
	</head>
	<body>
		<!-- it will allow us to create a session accessable accross our pages -->
		<?php session_start(); ?>
		<a href="./trial.php">Trial page</a>

		<?php $_SESSION['trial'] = 'trial eta';?>
		<?php echo $_SESSION['trial']; ?>


		<h3>Add Task</h3>

		<form method="POST" >
            <input type="hidden" name="action" value="add"/>
            Description: <input type="text" name="description" required/>
            <button type="submit">Add</button>
        </form>

	</body>
</html>